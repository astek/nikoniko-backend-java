/**
 * This file is part of NikoNiko.
 *
 * NikoNiko is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NikoNiko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NikoNiko.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.dao.configuration;

import javax.annotation.PostConstruct;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * DbManager for couchDB connection.
 *
 * @author odupre
 */
//@Service
public class DbManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbManager.class);

    @Autowired
    private AbstractCouchDbConfiguration couchDbConfiguration;

    /**
     * db connection
     */
    private CouchDbConnector db;

    @PostConstruct
    protected void dbInit() {
        LOGGER.info("Instanciating DbManager for db {}:{}/{}", couchDbConfiguration.getHost(), couchDbConfiguration.getPort(),
                couchDbConfiguration.getDatabaseName());

        final StdHttpClient.Builder httpClientBuilder = new StdHttpClient.Builder()
                .host(couchDbConfiguration.getHost())
                .port(Integer.parseInt(couchDbConfiguration.getPort()));

        if (couchDbConfiguration.getUsername().isPresent() && couchDbConfiguration.getPassword().isPresent()) {
            LOGGER.trace("Connection to Db with Username {}", couchDbConfiguration.getUsername());
            httpClientBuilder.username(couchDbConfiguration.getUsername().get());
            httpClientBuilder.password(couchDbConfiguration.getPassword().get());
        }

        final HttpClient httpClient = httpClientBuilder.build();

        final CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
        LOGGER.trace("dbInstance retrieved.");

        db = new StdCouchDbConnector(couchDbConfiguration.getDatabaseName(), dbInstance);

        LOGGER.trace("Db connector instanciated");

        db.createDatabaseIfNotExists();
    }

    /**
     * getDb
     *
     * @return CouchDbConnector
     */
    public CouchDbConnector getDb() {
        return db;
    }
}
