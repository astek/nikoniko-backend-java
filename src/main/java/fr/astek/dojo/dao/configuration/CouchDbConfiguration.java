/**
 * This file is part of NikoNiko.
 *
 * NikoNiko is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NikoNiko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NikoNiko.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.dao.configuration;

import org.springframework.context.annotation.Configuration;

/**
 * Database configuration.
 * 
 * TODO Update CONTEXT_NAME
 *
 * @author Astek
 */
//@Configuration
public class CouchDbConfiguration extends AbstractCouchDbConfiguration {

    /**
     * Couchdb JNDI context. TODO Update this field.
     */
    public static final String CONTEXT_NAME = "couchdb/nikoniko-backend";

    public CouchDbConfiguration() {
        this(CONTEXT_NAME);
    }

    public CouchDbConfiguration(final String dbName) {
        super(dbName);
    }
}