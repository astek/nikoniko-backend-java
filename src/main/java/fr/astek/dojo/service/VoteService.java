/**
 * This file is part of NikoNiko.
 *
 * NikoNiko is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NikoNiko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NikoNiko.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.service;

import fr.astek.dojo.dao.VoteRepository;
import fr.astek.dojo.model.Vote;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author odupre
 */
@Service
public class VoteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VoteService.class);

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private DateService dateService;

    public void vote(final Vote vote) {
        voteRepository.getVotes().add(vote);
    }

    /**
     *
     * @return
     */
    public List<Vote> getAll() {
        return voteRepository.getVotes();
    }

    public List<Vote> byDates(final LocalDate from, final LocalDate to, final Optional<List<Vote>> votes) {
        final List<Vote> allVotes = votes.orElse(getAll());
        final List<Vote> selectedVotes = new ArrayList<>();

        allVotes.stream().forEach((vote) -> {
            final LocalDate selectedDate = dateService.getLocalDateFromTimeInMillis(vote.getDate());
            if (selectedDate.isAfter(from) && selectedDate.isBefore(to)) {
                selectedVotes.add(vote);
            }
        });

        return selectedVotes;
    }

    public List<Vote> byUser(final String user, final Optional<List<Vote>> votes) {
        final List<Vote> allVotes = votes.orElse(getAll());
        final List<Vote> selectedVotes = new ArrayList<>();

        allVotes.stream().forEach((vote) -> {
            if (user.equals(vote.getUserName())) {
                selectedVotes.add(vote);
            }
        });

        return selectedVotes;
    }
}
