/**
 * This file is part of NikoNiko.
 *
 * NikoNiko is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NikoNiko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NikoNiko.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import org.springframework.stereotype.Service;

/**
 * @author odupre
 */
@Service
public class DateService {

    public LocalDate getLocalDateFromTimeInMillis(final Long timeInMillis) {
        return getLocalDateTimeFromTimeInMillis(timeInMillis).toLocalDate();
    }

    public LocalDateTime getLocalDateTimeFromTimeInMillis(final Long timeInMillis) {
        final Instant instant = Instant.ofEpochMilli(timeInMillis);
        final LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());

        return localDateTime;
    }
}
