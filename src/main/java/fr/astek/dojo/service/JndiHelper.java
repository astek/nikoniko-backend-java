/**
 * This file is part of NikoNiko.
 *
 * NikoNiko is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NikoNiko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NikoNiko.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.service;

import java.util.Optional;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameterized Helper to get anything out of JNDI.
 *
 * @author ttsing
 */
public class JndiHelper<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JndiHelper.class);

    private static final String JAVA_CONTEXT = "java:";
    private static final String INITIAL_CONTEXT = JAVA_CONTEXT + "comp/env";

    public Optional<T> getElementFromContext(final String contextName) throws NamingException {
        final Context initialContext = new InitialContext();
        // Get the Environement variable (from the global context) identified by its name
        Optional<T> elementFromContext;
        // Try one after the other different JNDI configuration (depending on wether we are deploying on Tomcat or WebSphere Liberty)
        try {
            // Get the global context
            final Context environmentContext = (Context) initialContext.lookup(INITIAL_CONTEXT);
            elementFromContext = Optional.of((T) environmentContext.lookup(contextName));
            LOGGER.info("Successfully getting element from JNDI context: {}/{}: {}", INITIAL_CONTEXT, contextName, elementFromContext.get());
        } catch (NamingException e) {
            LOGGER.warn(String.format("Problem %s while accessing context %s/%s. Trying with another context.", e.getMessage(), INITIAL_CONTEXT, contextName));
            try {
                elementFromContext = Optional.of((T) initialContext.lookup(JAVA_CONTEXT + contextName));
                LOGGER.info("Successfully getting element from second JNDI context: {}{}: {}", JAVA_CONTEXT, contextName, elementFromContext.get());
            } catch (NamingException e2) {
                LOGGER.warn(String.format("Problem %s while accessing context %s%s. Trying with a third context.", e2.getMessage(), JAVA_CONTEXT, contextName));
                try {
                    elementFromContext = Optional.of((T) initialContext.lookup(contextName));
                    LOGGER.info("Successfully getting element from third JNDI context: {}: {}", contextName, elementFromContext.get());
                } catch (Exception e3) {
                    LOGGER.error(String.format("Error %s while accessing context %s", e3.getMessage(), contextName), e3);
                    elementFromContext = Optional.empty();
                }
            }
        }

        return elementFromContext;
    }
}
