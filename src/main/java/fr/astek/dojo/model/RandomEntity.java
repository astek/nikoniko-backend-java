/**
 * This file is part of NikoNiko.
 *
 * NikoNiko is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NikoNiko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NikoNiko.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.model;

import java.io.Serializable;
import java.security.SecureRandom;
import java.util.Objects;
import java.util.UUID;

/**
 * Représentaiton d'un Vote.
 *
 * @author odupre
 * @version $Id: $
 */
public class RandomEntity implements Serializable {

    private Integer randomInt;
    private Double randomDouble;
    private UUID uid;

    public RandomEntity() {
        this(new SecureRandom().nextInt(), Math.random(), UUID.randomUUID());
    }

    public RandomEntity(Integer randomInt, Double randomDouble, UUID uid) {
        this.randomInt = randomInt;
        this.randomDouble = randomDouble;
        this.uid = uid;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.randomInt);
        hash = 71 * hash + Objects.hashCode(this.randomDouble);
        hash = 71 * hash + Objects.hashCode(this.uid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RandomEntity other = (RandomEntity) obj;
        if (!Objects.equals(this.randomInt, other.randomInt)) {
            return false;
        }
        if (!Objects.equals(this.randomDouble, other.randomDouble)) {
            return false;
        }
        
        return Objects.equals(this.uid, other.uid);
    }

    @Override
    public String toString() {
        return "RandomEntity{" + "randomInt=" + randomInt + ", randomDouble=" + randomDouble + ", uid=" + uid + '}';
    }

    public Integer getRandomInt() {
        return randomInt;
    }

    public void setRandomInt(Integer randomInt) {
        this.randomInt = randomInt;
    }

    public Double getRandomDouble() {
        return randomDouble;
    }

    public void setRandomDouble(Double randomDouble) {
        this.randomDouble = randomDouble;
    }

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }

}
