/**
 * This file is part of NikoNiko.
 *
 * NikoNiko is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NikoNiko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NikoNiko.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * Représentaiton d'un Vote.
 *
 * @author odupre
 * @version $Id: $
 */
public class Vote implements Serializable {

    private VoteValue voteValue;
    private String userName;
    private Long date;
    private UUID uid;

    public Vote() {
    }

    public Vote(final VoteValue voteValue, final String userName, final Long date, final UUID uid) {
        this.voteValue = voteValue;
        this.userName = userName;
        this.date = date;
        this.uid = uid;
    }

    public VoteValue getVoteValue() {
        return voteValue;
    }

    public void setVoteValue(VoteValue voteValue) {
        this.voteValue = voteValue;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }
}
