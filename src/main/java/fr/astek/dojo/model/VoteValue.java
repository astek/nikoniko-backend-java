/**
 * This file is part of NikoNiko.
 *
 * NikoNiko is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NikoNiko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NikoNiko.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.model;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The higher, the better.
 * 
 * @author odupre
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum VoteValue {
    MAD(1), SAD(2), GLAD(3);

    private final int value;

    private VoteValue(final int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
