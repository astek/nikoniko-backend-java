package fr.astek.dojo.controller;

import fr.astek.dojo.model.Vote;
import fr.astek.dojo.service.DateService;
import fr.astek.dojo.service.VoteService;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller Rest to vote... <br/>
 * Comme tous les Controller Rest, l'objectif est JUSTE d'être un passe plat vers le service. Ce qui permet d'externaliser les services, de les réutiliser,
 * etc... Exemple.<br/>
 *
 * @author gduvinage
 * @version $Id: $
 * @see https://spring.io/guides/gs/rest-service-cors/
 */
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/votes")
public class VoteController {

    private static final Logger LOGGER = LoggerFactory.getLogger(VoteController.class);

    @Autowired
    private VoteService voteService;

    @Autowired
    private DateService dateService;

    @RequestMapping(method = GET)
    @ResponseBody
    public List<Vote> list() {
        return voteService.getAll();
    }

    @RequestMapping(method = GET, path = "/{from}/{to}")
    @ResponseBody
    public List<Vote> byDates(@PathVariable final String from, @PathVariable final String to) throws ParseException {
        final LocalDate fromDate = dateService.getLocalDateFromTimeInMillis(Long.valueOf(from));
        final LocalDate toDate = dateService.getLocalDateFromTimeInMillis(Long.valueOf(to));

        return voteService.byDates(fromDate, toDate, Optional.empty());
    }

    @RequestMapping(method = GET, path = "/{user}")
    @ResponseBody
    public List<Vote> byUser(@PathVariable final String user) {
        return voteService.byUser(user, Optional.empty());
    }

    @RequestMapping(method = GET, path = "/{from}/{to}/{user}")
    @ResponseBody
    public List<Vote> byDatesAndUser(@PathVariable final String from,
            @PathVariable final String to,
            @PathVariable final String user) throws ParseException {
        final LocalDate fromDate = dateService.getLocalDateFromTimeInMillis(Long.valueOf(from));
        final LocalDate toDate = dateService.getLocalDateFromTimeInMillis(Long.valueOf(to));

        final List<Vote> selectedVotes = voteService.byDates(fromDate, toDate, Optional.empty());

        return voteService.byUser(user, Optional.of(selectedVotes));
    }
    
    @RequestMapping(method = POST)
    @ResponseBody
    public void vote(@RequestBody Vote vote) {
        LOGGER.info("Vote is {}\nuser name is {}\nGiven date is {}\nConverted date is {}", vote.getVoteValue(), vote.getUserName(), vote.getDate(),
                dateService.getLocalDateFromTimeInMillis(vote.getDate()));
        voteService.vote(vote);
    }
}
