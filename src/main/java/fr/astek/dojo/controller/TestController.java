/*
 * Copyright (C) 2016 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.controller;

import fr.astek.dojo.model.RandomEntity;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Service Rest de test.
 *
 * @author odupre
 */
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/tests")
public class TestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);
    
    private static final Integer MAX_ELEMENT = 20;
    private static final Integer MAX_DELAY = 5000;

    @RequestMapping(method = GET)
    @ResponseBody
    public List<RandomEntity> list() throws InterruptedException {
        int nbEntity = new SecureRandom().nextInt(MAX_ELEMENT) + 1;
        LOGGER.info("Nb element created: {}", nbEntity);

        final List<RandomEntity> entityList = new ArrayList<>(nbEntity);
        for (int i = 0; i < nbEntity; i++) {
            entityList.add(new RandomEntity());
        }

        long delay = new SecureRandom().nextInt(MAX_DELAY);
        LOGGER.info("Delay: {}", delay);
        Thread.sleep(delay);

        return entityList;
    }
}
