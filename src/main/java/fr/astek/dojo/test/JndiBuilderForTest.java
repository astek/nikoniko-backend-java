/**
 * This file is part of NikoNiko.
 *
 * NikoNiko is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NikoNiko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NikoNiko.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.test;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

/**
 * <b>This class is for Integration Tests only</b>. This class setUp the JNDI context before each other action to provide JNDI context to the (at)Configuration
 * (CouchDbConfiguration.class). The @Configuration is instantiated before all, so the spring context is not yet available.
 */
public class JndiBuilderForTest {

    private final static Logger LOGGER = LoggerFactory.getLogger(JndiBuilderForTest.class);

    public final static String COUCHDB_PROPERTIES_FILE = "couchdb-dev.properties";

    private final static String INITIAL_CONTEXT = "java:comp/env";
    public final static String COUCHDB_CONTEXT = "couchdb/UNKNOWN";

    /**
     * Call this method with appropriate path + name for your couchdb properties file, relative to test resources folder.
     *
     * @param couchDbPropertiesFilePath the file path + name, relative to test resources folder, containing configuration for you test DB.
     * <code>Optional.empty()</code> to use default name : <code>couchdb-dev.properties</code>.
     * @param contextName the name of the context to create in initial. e.g. <code>Optional.of("couchdb/emodule")</code> to create the context
     * <code>java:comp/env/couchdb/emodule</code>.
     */
    public static void setUp(final Optional<String> couchDbPropertiesFilePath, final Optional<String> contextName) {
        try {
            // Init a naming context. JNDI context failed to start if these 2 lines are missing
            final SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
            builder.activate();

            LOGGER.info("Initialization of JNDI context for tests");

            // Init the JNDI context for our tests
            final Context environmentContext = new InitialContext();

            final String fileName = couchDbPropertiesFilePath.orElse(COUCHDB_PROPERTIES_FILE);
            final File couchDbPropertiesFile = new File(ClassLoader.getSystemResource(fileName).getFile());
            LOGGER.debug("File used to load CouchDb properties: {}", couchDbPropertiesFile.getPath());
            environmentContext.bind(contextName.orElse(COUCHDB_CONTEXT), couchDbPropertiesFile.getPath());
            LOGGER.info("Context {} initialized with resource {}", contextName.orElse(COUCHDB_CONTEXT), fileName);

            final Context initialContext = new InitialContext();
            initialContext.bind(INITIAL_CONTEXT, environmentContext);
            LOGGER.info("Context {}/{} initialized properly", INITIAL_CONTEXT, contextName.orElse(COUCHDB_CONTEXT));
        } catch (IllegalStateException | NamingException exception) {
            LOGGER.info(String.format("The initialization of %s/%s context as failed", INITIAL_CONTEXT, contextName.orElse(COUCHDB_CONTEXT)), exception);
        }
    }

    /**
     * Call this method with appropriate path + name for your couchdb properties file, relative to test resources folder.
     *
     * @param lookupJndi the object you are expecting from JNDI.
     * @param contextName the name of the context to create in initial. e.g. <code>Optional.of("couchdb/emodule")</code> to create the context
     * <code>java:comp/env/couchdb/emodule</code>.
     */
    public static void setUp(final Object lookupJndi, final Optional<String> contextName) {
        try {
            // Init a naming context. JNDI context failed to start if these 2 lines are missing
            final SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
            builder.activate();

            LOGGER.info("Initialization of JNDI context for tests");

            // Init the JNDI context for our tests
            final Context environmentContext = new InitialContext();

            environmentContext.bind(contextName.orElse(COUCHDB_CONTEXT), lookupJndi);
            LOGGER.info("Context {} initialized with resource {}", contextName.orElse(COUCHDB_CONTEXT), lookupJndi);

            final Context initialContext = new InitialContext();
            initialContext.bind(INITIAL_CONTEXT, environmentContext);
            LOGGER.info("Context {}/{} initialized properly", INITIAL_CONTEXT, contextName.orElse(COUCHDB_CONTEXT));
        } catch (IllegalStateException | NamingException exception) {
            LOGGER.info(String.format("The initialization of %s/%s context as failed", INITIAL_CONTEXT, contextName.orElse(COUCHDB_CONTEXT)), exception);
        }
    }

    /**
     * Call this method with appropriate map with key is contextName and value is valueOfContext.
     *
     * @param keyContextMap map with key is contextName and value is valueOfContext
     */
    public static void setUpWithMap(final Map<String, String> keyContextMap) {
        try {
            // Init a naming context. JNDI context failed to start if these 2 lines are missing
            final SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
            builder.activate();

            LOGGER.info("Initialization of JNDI context for tests");

            final Context environmentContext = new InitialContext();

            final StringBuilder context = new StringBuilder();

            for (Entry<String, String> entry : keyContextMap.entrySet()) {
                environmentContext.bind(entry.getKey(), entry.getValue());
                context.append(entry.getKey());
            }

            final Context initialContext = new InitialContext();
            initialContext.bind(INITIAL_CONTEXT, environmentContext);
            LOGGER.info("Context {}{} initialized properly", INITIAL_CONTEXT, context.toString());
        } catch (IllegalStateException | NamingException exception) {
            LOGGER.info(exception.getMessage(), exception);
        }
    }
}
