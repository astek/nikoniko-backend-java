/**
 * This file is part of NikoNiko.
 *
 * NikoNiko is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NikoNiko is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NikoNiko.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.astek.dojo.service;

import fr.astek.dojo.dao.VoteRepository;
import fr.astek.dojo.model.Vote;
import fr.astek.dojo.model.VoteValue;
import java.util.ArrayList;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertEquals;

/**
 * TU du service de Vote.
 *
 * @author odupre
 */
@RunWith(MockitoJUnitRunner.class)
public class VoteServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(VoteServiceTest.class);

    @InjectMocks
    private VoteService voteService;
    
    @Mock
    private VoteRepository voteRepository;

    /**
     * Test of stupidService method, of class BookService.
     */
    @Test
    public void testAdd1Vote() {
        LOGGER.trace("testAdd1Vote");
        Mockito.when(voteRepository.getVotes()).thenReturn(new ArrayList<>());

        final Vote expectedVote = new Vote(VoteValue.MAD, "foobar", 145278730002L, new UUID(0, 0));
        voteService.vote(expectedVote);

        assertEquals(1, voteService.getAll().size());
        assertEquals(expectedVote, voteService.getAll().get(0));
    }
}
