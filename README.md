## Compilation and Tests
Compile and test it with Maven...
    `mvn clean install`

## Run it
Run it with Java and Tomcat Embed
    `java -jar target/nikoniko-backend.war`
    
## Make it faster...
Or compile, test and run it with the magic one line command:
    `mvn spring-boot:run`

## Use it
Services are listed with Swagger.
http://localhost:8080/swagger-ui.html
coucou
